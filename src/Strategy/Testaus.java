/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Strategy;

import static java.lang.Thread.sleep;

/**
 *
 * @author Varpe
 */
public class Testaus {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        Context algo = new Context(new SelectionStrategy());
        algo.createContext();
        algo.sorttaus();
        System.out.println("Vaihto shakeriin");
        Thread.sleep(4000);
        algo.setStrategy(new ShakerStrategy());
        algo.sorttaus();
        System.out.println("Vaihto insertioniin");
        Thread.sleep(4000);
        algo.setStrategy(new InsertionStrategy());
        algo.sorttaus();
    }
    
}
