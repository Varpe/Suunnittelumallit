/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Strategy;

import java.util.List;

/**
 *
 * @author Varpe
 */
public class SelectionStrategy implements SortingStrategy {

    @Override
    public void getSortedList(int[] a) {
        for (int i = 0; i < a.length; i++) {
	    int min = i;
            int j;

            
            for (j = i + 1; j < a.length; j++) {

		if (a[j] < a[min]) {
                    min = j;
                }
	    }

            
            int T = a[min];
            a[min] = a[i];
	    a[i] = T;
        }
      
    
}}