/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Strategy;

import java.util.List;

/**
 *
 * @author Varpe
 */
public class ShakerStrategy implements SortingStrategy{

    @Override
    public void getSortedList(int[] a) {
        int i = 0;
	int k = a.length - 1;
	while (i < k) {
	    int min = i;
	    int max = i;
            int j;
            for (j = i + 1; j <= k; j++) {
		if (a[j] < a[min]) {
                    min = j;
                }
		if (a[j] > a[max]) {
                    max = j;
                }
	    }
            int T = a[min];
            a[min] = a[i];
	    a[i] = T;

	    if (max == i) {
	        T = a[min];
	        a[min] = a[k];
	 	a[k] = T;
	    } else {
	        T = a[max];
	        a[max] = a[k];
	        a[k] = T;
	    }
	    i++;
	    k--;
        }
    } 
}
