/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Strategy;



/**
 *
 * @author Varpe
 */
public class InsertionStrategy implements SortingStrategy {

    @Override
    public void getSortedList(int[] a) {
        final float SHRINKFACTOR = (float)1.3;
        boolean flipped = false;
        int gap, top;
        int i, j;

        gap = a.length;
        do {
            gap = (int) ((float) gap / SHRINKFACTOR);
            switch (gap) {
            case 0: 
                gap = 1;
                break;
            case 9: 
            case 10: 
                gap = 11;
                break;
            default: break;
            }
            flipped = false;
            top = a.length - gap;
            for (i = 0; i < top; i++) {

                j = i + gap;
                if (a[i] > a[j]) {
                    int T = a[i];
                    a[i] = a[j];
                    a[j] = T;
                    flipped = true;
                }
            }
        } while (flipped || (gap > 1));
        
    } 
}
