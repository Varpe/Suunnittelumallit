/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Strategy;

import java.util.Random;

/**
 *
 * @author Varpe
 */
public class Context {
    private int[] a;
    private SortingStrategy strategy;
    
    public Context(SortingStrategy strategy){
       this.strategy = strategy;
       a = new int[100000];
       createContext();
    }
    
    public void setStrategy(SortingStrategy strategy){
        this.strategy = strategy;
    }
    
    public void createContext(){
        Random rand = new Random();
        for(int i=0;i<100000;i++){
            int lisättävä = rand.nextInt(10000 - 0 + 1);
            a[i] = lisättävä;
            
        }
    
    }
    public void sorttaus(){
        strategy.getSortedList(a);
        for(int i=0;i<a.length;i++){
            System.out.println(a[i]);
        }
    }
}