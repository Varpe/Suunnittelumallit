package factorymethod;

public class Main {

    public static void main(String[] args) {
        AterioivaOtus opettaja = new Opettaja();
        AterioivaOtus opiskelija = new Opiskelija();
        AterioivaOtus vartija = new Vartija();
        opettaja.aterioi();
        opiskelija.aterioi();
        vartija.aterioi();
    }
}
