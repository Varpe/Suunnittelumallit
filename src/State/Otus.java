/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package State;

/**
 *
 * @author Varpe
 */
public class Otus {
    private int taso;
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    private OtusState otusState;
    
    public Otus(int taso){
        this.setTaso(taso);
        }
    
    public void hyökkäys(){
        otusState.hyökkäys();
        
    }
    public void hyppy(){
        otusState.hyppy();
    }
    public void kerroTiedot(){
        otusState.kerroTiedot();
    }
    
    public void setTaso(int taso){
       this.taso=taso;
        if(taso==1){
            otusState = new TasoYksi();
        }else if(taso==2){
            otusState = new TasoKaksi();
        }else if(taso==3){
            otusState = new TasoKolme();
        }else{
            otusState = new TasoYksi();
        } 
    }
}
