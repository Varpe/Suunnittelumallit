/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ChainOfResponsibility;

/**
 *
 * @author Varpe
 */
abstract class PäätösVoima {
    protected static final double voima = 1;
    protected PäätösVoima successor;
    
    public void setSuccessor(PäätösVoima successor){
        this.successor = successor;
    }
    
    abstract public void processRequest(KorotusRequest request);
    
}
