/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ChainOfResponsibility;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author Varpe
 */
public class Testi {
    public static void main(String[] args) {
        Lähiesimies lähiesimies = new Lähiesimies();
        Päällikkö päällikkö = new Päällikkö();
        Toimitusjohtaja toimitusjohtaja = new Toimitusjohtaja();
        lähiesimies.setSuccessor(päällikkö);
        päällikkö.setSuccessor(toimitusjohtaja);
        
        try{
            while(true){
                System.out.println("Anna palkankorotus toiveesi prosenttina (0,01 = 1%), niin katsotaan kuka hyväksyy sen:");
            double d = Double.parseDouble(new BufferedReader(new InputStreamReader(System.in)).readLine());
            lähiesimies.processRequest(new KorotusRequest(d,"Yleinen"));
        }}
            catch(Exception e){
                    System.exit(1);
                    }
    }
    
}
