/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ChainOfResponsibility;

/**
 *
 * @author Varpe
 */
public class Toimitusjohtaja extends PäätösVoima{
        private final double sallittu = voima;
    @Override
    public void processRequest(KorotusRequest request) {
        if(request.getMäärä() > 0.05 && request.getMäärä()<=sallittu){
            System.out.println("Toimitusjohtaja hyväksyy $"+request.getMäärä());
        }else if (successor != null){
            successor.processRequest(request);
        }
    }
    
}
