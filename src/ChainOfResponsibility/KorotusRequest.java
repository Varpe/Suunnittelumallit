/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ChainOfResponsibility;

/**
 *
 * @author Varpe
 */
public class KorotusRequest {
    private double määrä;
    private String syy;
    
    public KorotusRequest(double määrä, String syy){
        this.määrä = määrä;
        this.syy = syy;
    }
    
    public double getMäärä(){
        return määrä;
    }
    public void setMäärä(double mä){
        määrä = mä;
    }
    public String getSyy(){
        return syy;
    }
    public void setPurpose(String syytin){
        syy = syytin;
    }
    
}
