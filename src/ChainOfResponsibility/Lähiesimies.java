/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ChainOfResponsibility;

/**
 *
 * @author Varpe
 */
public class Lähiesimies extends PäätösVoima{
    private final double sallittu = voima*0.02;

    @Override
    public void processRequest(KorotusRequest request) {
        if(request.getMäärä() <= sallittu){
            System.out.println("Lähiesimies hyväksyy $" + request.getMäärä());
        }else if (successor != null){
            successor.processRequest(request);
        }
    }
    
}
