/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ChainOfResponsibility;

/**
 *
 * @author Varpe
 */
public class Päällikkö extends PäätösVoima {
    private final double sallittu = 0.05*voima;
    
    @Override
    public void processRequest(KorotusRequest request) {
        if(request.getMäärä() > 0.02 && request.getMäärä() < sallittu){
            System.out.println("Päällikkö hyväksyy $" + request.getMäärä());
        }else if (successor != null){
            successor.processRequest(request);
        }
    }
    
}
