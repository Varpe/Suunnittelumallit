/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bridge;

/**
 *
 * @author Varpe
 */
abstract class Laite {
    public int laiteState;
    public int maxAsetus;
    public int volumeTaso = 0;
    
    public abstract void näppäinViisiPainettu();
    public abstract void näppäinKuusiPainettu();
    
    public void laitePalauta(){
        if(laiteState > maxAsetus || laiteState < 0){ laiteState = 0; }
            System.out.println("Päällä: " + laiteState);
        }
    
    public void näppäinSeitsemänPainettu(){
        volumeTaso++;
        System.out.println("Volume = " + volumeTaso);

    
}
    public void näppäinKahdeksanPainettu(){
        volumeTaso--;
        System.out.println("Volume = " + volumeTaso);

    
}
}