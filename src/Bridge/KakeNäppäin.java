/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bridge;

/**
 *
 * @author Varpe
 */
public abstract class KakeNäppäin {
    private Laite laite;
    
    public KakeNäppäin(Laite uusiLaite){
       laite = uusiLaite;
    }
    
    public void näppäinViisiPainettu(){
        laite.näppäinViisiPainettu();
    }
    public void näppäinKuusiPainettu(){
        laite.näppäinKuusiPainettu();
    }
    public void laitePalaute(){
        laite.laitePalauta();
    }
    
    public abstract void näppäinYhdeksänPainettu();
    
}
