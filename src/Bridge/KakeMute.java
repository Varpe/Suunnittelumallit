/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bridge;

/**
 *
 * @author Varpe
 */
public class KakeMute extends KakeNäppäin{

    public KakeMute(Laite uusiLaite) {
        super(uusiLaite);
    }
    
    public void näppäinYhdeksänPainettu(){
        System.out.println("Televisio asetettiin äänettömälle");
    }
    
}
