/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bridge;

/**
 *
 * @author Varpe
 */
public class Test {
    public static void main(String[] args) {
        KakeNäppäin tv = new KakeMute(new Televisio(1,200));
        KakeNäppäin tv2 = new KakePause(new Televisio(1,200));
        
        System.out.println("Testataan television asettaminen äänettömälle");
        
        tv.näppäinViisiPainettu(); //Kanava pienemmälle
        tv.näppäinKuusiPainettu(); //Kanava isommalle
        tv.näppäinYhdeksänPainettu(); //Televisio äänettömälle
        tv.laitePalaute();  //Tämänhetkinen kanava
        
        System.out.println("Testaa television pausettamista");
        
        tv2.näppäinViisiPainettu();  //Kanava pienemmälle
        tv2.näppäinKuusiPainettu();  //Kanava isommalle
        tv2.näppäinKuusiPainettu(); //Kanava isommalle
        tv2.näppäinKuusiPainettu(); //Kanava isommalle
        tv2.näppäinKuusiPainettu(); //Kanava isommalle
        tv2.näppäinYhdeksänPainettu(); //Televisio pauselle
        tv2.laitePalaute(); //Tämänhetkinen kanava
        
    }
    
}
