/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bridge;

/**
 *
 * @author Varpe
 */
public class Televisio extends Laite {
    
    public Televisio(int newLaiteState, int newMaxAsetus){
        laiteState = newLaiteState;
        maxAsetus = newMaxAsetus;
    }

    @Override
    public void näppäinViisiPainettu() {
        System.out.println("Vaihda kanavaa alaspäin");
        laiteState--;
    }

    @Override
    public void näppäinKuusiPainettu() {
        System.out.println("Vaihda kanavaa ylöspäin");
        laiteState++;
    }
    
}
