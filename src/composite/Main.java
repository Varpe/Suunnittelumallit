/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite;

/**
 *
 * @author Varpe
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Näytönohjain näytönohjain = new Näytönohjain();
        Muistipiiri muistipiiri = new Muistipiiri();
        Verkkokortti verkkokortti = new Verkkokortti();
        Emolevy emolevy = new Emolevy();
        Kotelo kotelo = new Kotelo();
        Prosessori prosessori = new Prosessori();
        
        Tietokone tietokone = new Tietokone();
        
        tietokone.add(näytönohjain);
        tietokone.add(muistipiiri);
        tietokone.add(verkkokortti);
        tietokone.add(emolevy);
        tietokone.add(kotelo);
        tietokone.add(prosessori);
    }
    
}
