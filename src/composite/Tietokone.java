/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Varpe
 */
public class Tietokone implements Laiteosa{
    List<Laiteosa> osalista = new ArrayList<Laiteosa>();
    public void addOsa(Laiteosa laiteosa){
        osalista.add(laiteosa);
    }
    public void print(){
        for(Laiteosa l: osalista){
            l.print();
           
        }
    }
    public void add(Laiteosa laiteosa){
        osalista.add(laiteosa);
    }
    public void remove(Laiteosa laiteosa){
        osalista.remove(laiteosa);
    }
    
}
