/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TemplateMethod;
import java.util.Random;
/**
 *
 * @author Varpe
 */
public class MetropoliaPeli extends Peli {

    @Override
    void initializeGame() {
        int[] playerPositions = new int[playersCount];
        for (int i = 0; i< playersCount; i++){
            playerPositions[i] = 0;
        }
        noppa = new Random();
        
        int voittajaId = -1;
    }

    @Override
    void makePlay(int player) {
        int noppaHeitto = noppa.nextInt(6) + 1;
        
        playerPositions[player] += noppaHeitto;
        
        int penaltyOrBonus = board[playerPositions[player]];
        playerPositions[player] += penaltyOrBonus;
        
        if (playerPositions[player] > 8){
            voittajaId = player;
        }
    }

    @Override
    boolean endOfGame() {
        return (voittajaId != -1);
    }

    @Override
    void printWinner() {
        System.out.println("Voittaja on pelaaja numero " + voittajaId);
    }
    private static final int[] board = {0, 0, -1, 0, 3, 0, 0, 0, -5, 0};
    private int[] playerPositions = null;
    private Random noppa = null;
    private int voittajaId = -1;
}
