/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Varpe
 */
public class mySingleton extends JFrame{
    
    private static mySingleton uniqueInstance;
    private static JPanel panel;
    private static JLabel label;
    private static String str="";
    
    private mySingleton(){
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setPreferredSize(new Dimension(200,100));
        pack();
        setLocation(750,280);
        java.util.Date now = new java.util.Date();
        str = now.toString();
        label = new JLabel(str);
        add(label);
        setVisible(true);
    }
    
    public static mySingleton getInstance(){
        if(uniqueInstance==null){
            uniqueInstance = new mySingleton();
        }
        return uniqueInstance;
    }
    
}
