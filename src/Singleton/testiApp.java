/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Varpe
 */
public class testiApp extends JPanel implements ActionListener{
    JButton button;
    
    public testiApp(){
        setPreferredSize(new Dimension(200,50));
        JButton button = new JButton("avaa");
        button.addActionListener(this);
        add(button);
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(new testiApp());
        frame.pack();
        frame.setLocation(700,380);
        frame.setVisible(true);
    }
    public void actionPerformed(ActionEvent e){
        mySingleton.getInstance();
    }
    
}
