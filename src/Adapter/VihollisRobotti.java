/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adapter;

import java.util.Random;

/**
 *
 * @author Varpe
 */
public class VihollisRobotti {
    Random random = new Random();
    
    public void lyöKäsillä(){
        int vahinko = random.nextInt(10)+1;
        System.out.println("Robotti tekee " + vahinko +" vahinkoa käsillään");
    }
    
    public void kävele(){
        int liiku = random.nextInt(5)+1;
        System.out.println("Robotti liikkuu "+ liiku+" metrin matkan");
    }
    
    public void kohtaaIhminen(String driverName){
        System.out.println("Robotti kävelee " + driverName + " ylitse");
    }
    
}
