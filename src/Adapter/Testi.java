/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adapter;

/**
 *
 * @author Varpe
 */
public class Testi {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        VihollisKopteri cessna = new VihollisKopteri();
        VihollisRobotti robo = new VihollisRobotti();
        Vihollinen robottiAdapter = new VihollisRobottiAdapter(robo);
        
        System.out.println("-Robotti-");
        robo.kävele();
        robo.kohtaaIhminen("Joonas");
        robo.lyöKäsillä();
        
        System.out.println("-Viholliskopteri-");
        
        cessna.asetaKuski("Arska");
        cessna.aja();
        cessna.hyökkää();
        
        System.out.println("-Robotti adapterilla-");
        robottiAdapter.asetaKuski("Heikki");
        robottiAdapter.aja();
        robottiAdapter.hyökkää();
        
        
    }
    
}
