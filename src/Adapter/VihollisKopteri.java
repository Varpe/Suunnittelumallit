/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adapter;

import java.util.Random;

/**
 *
 * @author Varpe
 */
public class VihollisKopteri implements Vihollinen{
    Random random = new Random();
    
    
    @Override
    public void hyökkää() {
        int vahinko = random.nextInt(10) + 1;
        System.out.println("Viholliskopteri tekee " + vahinko + " vahinkoa");
    }

    @Override
    public void aja() {
        int liiku = random.nextInt(5) + 1;
        System.out.println("Viholliskopteri liikkuu " + liiku + " metrin matkan");
    }

    @Override
    public void asetaKuski(String driverName) {
        System.out.println(driverName + " ajaa kopteria");
    }
    
}
