/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adapter;

/**
 *
 * @author Varpe
 */
public class VihollisRobottiAdapter implements Vihollinen{
    VihollisRobotti robotti;
    
    public VihollisRobottiAdapter(VihollisRobotti uusiRobotti){
        robotti = uusiRobotti;
    }
    
    @Override
    public void hyökkää() {
        robotti.lyöKäsillä();
    }

    @Override
    public void aja() {
        robotti.kävele();
    }

    @Override
    public void asetaKuski(String driverName) {
        robotti.kohtaaIhminen(driverName);
    }
    
}
