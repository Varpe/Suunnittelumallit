/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Builder;

import java.util.List;

/**
 *
 * @author Varpe
 */
public class HesburgerBuilder implements BurgerBuilder{
    private Burger burger;
    private List setit;
    
    public HesburgerBuilder(){
        this.burger = new Burger();
    }
    
    @Override
    public void buildBurgerPihvi() {
        burger.setBurgerPihvi("Hesen pihvi");
    }

    @Override
    public void buildBurgerKastike() {
        burger.setBurgerKastike("Hesen kastike");
    }

    @Override
    public void buildBurgerJuusto() {
        burger.setBurgerJuusto("Hesen juusto");
    }

    @Override
    public void buildBurgerLeipä() {
        burger.setBurgerLeipä("Hesen leipä");
    }
    
    
    @Override
    public List getBurger(){ 
        setit.add(this.burger);
        return setit;
    }
    
}
