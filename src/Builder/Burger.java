/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Builder;

/**
 *
 * @author Varpe
 */
public class Burger implements PurilaisPlan{
    private String Bleipä, Bpihvi, Bkastike, Bjuusto;

    @Override
    public void setBurgerLeipä(String leipä) {
        Bleipä = leipä;
    }
    
    public String getBurgerLeipä(){ return Bleipä; }

    @Override
    public void setBurgerPihvi(String pihvi) {
        Bpihvi = pihvi;
    }
    public String getBurgerPihvi(){ return Bpihvi; }

    @Override
    public void setBurgerKastike(String kastike) {
        Bkastike = kastike;
    }
    public String getBurgerKastike(){ return Bkastike; }
    

    @Override
    public void setBurgerJuusto(String juusto) {
        Bjuusto = juusto;
    }
    public String getBurgerJuusto(){ return Bjuusto; }
}
