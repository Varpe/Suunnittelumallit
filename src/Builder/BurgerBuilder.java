/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Builder;

import java.util.List;

/**
 *
 * @author Varpe
 */
public interface BurgerBuilder {
    public void buildBurgerPihvi();
    public void buildBurgerKastike();
    public void buildBurgerJuusto();
    public void buildBurgerLeipä();
    public List getBurger();
    
    
}
