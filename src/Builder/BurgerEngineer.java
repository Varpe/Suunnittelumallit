/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Builder;

import java.util.List;

/**
 *
 * @author Varpe
 */
public class BurgerEngineer {
    private BurgerBuilder burgerBuilder;
    
    public BurgerEngineer(BurgerBuilder burgerBuilder){
        this.burgerBuilder = burgerBuilder;
    }
    
    public List getBurger(){
        return this.burgerBuilder.getBurger();
    }
    
    public void makeBurger(){
        this.burgerBuilder.buildBurgerJuusto();
        this.burgerBuilder.buildBurgerKastike();
        this.burgerBuilder.buildBurgerLeipä();
        this.burgerBuilder.buildBurgerPihvi();
    }
}
