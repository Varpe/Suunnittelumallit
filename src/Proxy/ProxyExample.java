/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proxy;

import java.util.ArrayList;

/**
 *
 * @author Varpe
 */
public class ProxyExample {

   /**
    * Test method
    */
   public static void main(String[] args) {
        final Image IMAGE1 = new ProxyImage("HiRes_10MB_Photo1");
        final Image IMAGE2 = new ProxyImage("HiRes_10MB_Photo2");
        final Image IMAGE3 = new ProxyImage("HiRes_10MB_Photo3");
        final Image IMAGE4 = new ProxyImage("HiRes_10MB_Photo4");
       
        ArrayList<Image> valokuvat = new ArrayList();
        valokuvat.add(IMAGE1);
        valokuvat.add(IMAGE2);
        valokuvat.add(IMAGE3);
        valokuvat.add(IMAGE4);
        
        
        for(Image a:valokuvat) {
        a.showData();
   }
        IMAGE1.displayImage(); // tarvitaan lataus
        IMAGE1.displayImage(); // kuva on jo ladattu
        IMAGE2.displayImage(); // tarvitaan lataus
        IMAGE2.displayImage(); // kuva on jo ladattu
        IMAGE1.displayImage(); // kuva on jo ladattu
    }

}
