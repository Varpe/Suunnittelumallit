/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

/**
 *
 * @author Varpe
 */
public class KangasAlasCommand implements Command {
    private Valkokangas valkokangas;
    
    public KangasAlasCommand(Valkokangas valkokangas){
        this.valkokangas = valkokangas;
    }
    
    @Override
    public void execute(){
        valkokangas.kangasAlas();
    }
    
}
