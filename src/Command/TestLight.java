/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

/**
 *
 * @author Varpe
 */
public class TestLight {
    public static void main(String[] args) {
        Valkokangas valkokangas = new Valkokangas();
        Command switchUp = new KangasYlösCommand(valkokangas);
        Command switchDown = new KangasAlasCommand(valkokangas);
        KangasButton button1 = new KangasButton(switchUp);
        KangasButton button2 = new KangasButton(switchDown);
        
        button1.push();
        button2.push();
    }
    
}
