/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author Varpe
 */
public class BossTehdas implements Vaatetehdas {

    @Override
    public Lippis createLippis() {
       return new BossLippis();
    }

    @Override
    public Farkut createFarkut() {
        return new BossFarkut();
    }

    @Override
    public Paita createPaita() {
        return new BossPaita();
    }

    @Override
    public Kengät createKengät() {
        return new BossKengät();
    }
    @Override
    public String pue(){
       return "Jasper pukee päälleen: " + createFarkut()+" "+createKengät()+" "+createLippis()+" "+createPaita();
    }
    
}
