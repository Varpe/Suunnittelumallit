/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author Varpe
 */
public interface Vaatetehdas {
    
    public abstract Lippis createLippis();
    public abstract Farkut createFarkut();
    public abstract Paita createPaita();
    public abstract Kengät createKengät();
    public abstract String pue();
    
}
