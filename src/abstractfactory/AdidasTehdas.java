/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author Varpe
 */
public class AdidasTehdas implements Vaatetehdas {
    
    @Override
    public Lippis createLippis(){
        return new AdidasLippis();
    }
    
    @Override
    public Paita createPaita(){
        return new AdidasPaita();
        
    }
    @Override
    public Kengät createKengät(){
        return new AdidasKengät();
        
    }
    @Override
    public Farkut createFarkut(){
        return new AdidasFarkut();
        
    }
    
    @Override
    public String pue(){
       return "Jasper pukee päälleen: " + createFarkut()+" "+createKengät()+" "+createLippis()+" "+createPaita();
    }
}
