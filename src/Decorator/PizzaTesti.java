/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Decorator;

/**
 *
 * @author Varpe
 */
public class PizzaTesti {
    public static void main(String[] args) {
        Pizza kinkkuPizza = new Kinkku(new Juusto(new PerusPizza()));
        Pizza kebabPizza = new Kebab(new Juusto(new PerusPizza()));
        Pizza kebabSipuliPizza = new Sipuli(new Juusto(new Kebab(new PerusPizza())));
        
        
        System.out.println("Pizzeria Varpelaiden menu:");
        System.out.println("1. Kinkkupizza, täytteet: " + kinkkuPizza.getKuvaus() + " hinta: " + kinkkuPizza.getHinta()+ "euroa");
        System.out.println("2. Kebabpizza, täytteet:  " + kebabPizza.getKuvaus() + " hinta: " + kebabPizza.getHinta()+ "euroa");
        System.out.println("3. Speciale, täytteet:  " + kebabSipuliPizza.getKuvaus() + " hinta: " + kebabSipuliPizza.getHinta()+ "euroa");
    }
    
}
