/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Decorator;

/**
 *
 * @author Varpe
 */
public class Juusto extends TäyteDecorator {
    public Juusto(Pizza uusiPizza){
        super(uusiPizza);
    }
    
    
    public String getKuvaus(){
        return väliPizza.getKuvaus() + ", juusto";
    }
    
    public double getHinta(){
        return väliPizza.getHinta() + 1;
    }
}
