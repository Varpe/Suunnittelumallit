/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Decorator;

/**
 *
 * @author Varpe
 */
abstract class TäyteDecorator implements Pizza {
    protected Pizza väliPizza;
    
    public TäyteDecorator(Pizza uusiPizza){
        väliPizza = uusiPizza;
    }
    
    public String getKuvaus(){
        return väliPizza.getKuvaus();
    }
    
    public double getCost(){
        return väliPizza.getHinta();
    }
}
